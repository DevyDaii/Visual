import mymatplot
import numpy as np

# data = np.radom.choice(20,(100,10))

if __name__=="__main__":

    # 数据
    x_data = np.random.normal(0,1,20)
    y_data = np.random.normal(0,1,20)

    # 散列图
    mymatplot.scatterplot(x_data, y_data, x_label="测试x轴", y_label="测试y轴", title="测试")

    # 折线图
    #mymatplot.lineplot(x_data, y_data, x_label="测试x轴", y_label="测试y轴", title="测试折线图")